var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var livereload = require('gulp-livereload');
var sourcemaps = require('gulp-sourcemaps');

var sassPaths = [
  'bower_components/normalize.scss/sass',
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('sass', function() {
  return gulp.src(['scss/app.scss', 'scss/front.scss'], {base: 'scss/'})
    .pipe(sourcemaps.init())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compressed' // if css compressed **file size**
    })
    
    .on('error', $.sass.logError))
    
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    
    .pipe(sourcemaps.write())  // In browsers shows where the css code in sass file
    .pipe(gulp.dest('css'))
    .pipe(livereload());
});

gulp.task('default', ['sass'], function() {
  livereload.listen();
  gulp.watch(['scss/**/*.scss'], ['sass']);
});