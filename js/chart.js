d3.json('https://coinkapp.com/chart/prices', function (error, response) {
    d3.selectAll('div.chart-container').each(function () {
        var symbol = this.id.substr(6);
        if (symbol in response) {
            draw('#' + this.id, response[symbol]);
        }
    });
});

// Set the dimensions of the canvas / graph
var margin = {top: 20, right: 0, bottom: 0, left: 0},
    width = 400 - margin.left - margin.right,
    height = 150 - margin.top - margin.bottom;

var bisectDate = d3.bisector(function (d) {
        return d.date;
    }).left,
    formatValue = d3.format(",.2f"),
    formatDate = d3.timeFormat("%d %b %Y %-I:%M %p");
    formatCurrency = function (d) {
        return "$"+ formatValue(d);
    };

var svgs = d3.selectAll('div.chart-container')
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

var focus = svgs.append("g")
    .attr("class", "focus")
    .style("display", "none");

focus.append("circle")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
    .attr("r", 4.5);

var tooltipText = focus.append("text");

    tooltipText.append("tspan")
        .attr("font-size", '0.8em')
        .attr("x", 9)
        .attr("dy", "0em");

    tooltipText.append("tspan")
        .attr("font-size", '0.8em')
        .attr("x", 9)
        .attr("dy", ".85em");

// avatars
var avatarSize = 20;

var base = d3.selectAll("div.chart-container");
var canvas = base.append("canvas")
    .attr("width", width + margin.left + margin.right)
    .attr("height", avatarSize)
    .style("margin-top", '1px')
;

var draw = function (selector, data) {

    var svg = d3.select(selector).select('svg');
    var canvas = d3.select(selector).select('canvas');
    var focus = svg.select('g');
    g = svg
        .insert("g",":first-child")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var x = d3.scaleTime()
        .rangeRound([0, width]);

    var y = d3.scaleLinear()
        .rangeRound([height, 0]);

    var min = d3.min(data, function (d) {
        return d.price;
    });
    var area = d3.area()
        .x(function (d) {
            return x(d.date);
        })
        .y1(function (d) {
            return y(d.price);
        });

    var valueline = d3.line()
        .x(function (d) {
            return x(d.date);
        })
        .y(function (d) {
            return y(d.price);
        });

    x.domain(d3.extent(data, function (d) {
        return d.date;
    }));
    y.domain([min, d3.max(data, function (d) {
        return d.price;
    })]);

    area.y0(y(min));

    //area
    g.append("path")
        .datum(data)
        .attr("fill", "#009EE2")
        .attr("d", area);

    //line
    g.append("path")
        .data([data])
        .attr("class", "line")
        .attr("d", valueline);

    var context = canvas
        .node()
        .getContext("2d");

    //avatar
    var scale = d3.scaleTime()
        .range([0, width])
        .domain(d3.extent(data, function (d) {
            return d.date;
        }));

    for (var i = 0; i < data.length; i++) {
        if (data[i].comment !== null) {
            drawAvatar(scale(data[i].date), context, data[i].comment);
        }
    }

    svg.append("rect")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", function () {
            focus.style("display", null);
        })
        .on("mouseout", function () {
            focus.style("display", "none");
        })
        .on("mousemove", mousemove);

    var avatarBlock = d3
            .select(selector)
            .select(function() { return this.parentNode; })
            .select('div.commentPopover');

    function mousemove() {
        var x0 = x.invert(d3.mouse(this)[0]),
            i = bisectDate(data, x0, 1),
            d0 = data[i - 1],
            d1 = data[i],
            d = x0 - d0.date > d1.date - x0 ? d1 : d0;
        focus.attr("transform", "translate(" + x(d.date) + "," + y(d.price) + ")");
        focus.select("tspan:nth-child(1)").text(formatDate(d.date * 1000));
        focus.select("tspan:nth-child(2)").text(formatCurrency(d.price));

        //avatar update
        var avatar = avatarBlock.select("div.commentPopover__avatar");
        var username = avatarBlock.select('a.commentPopover__username');
        var comment = avatarBlock.select('p.commentPopover__body');

        if (d.comment !== null) {
            avatarBlock.classed('visible', true);
            avatar
                .style("margin-left", x(d.date) + "px")
                .style('background-image', 'url("' + d.comment[0] + '")')

            username
                .attr('href', '/u/' + d.comment[1])
                .html('<b>' + d.comment[1] + '</b>:');

            comment.text(d.comment[2]);

        } else {
            avatarBlock.classed('visible', false);
            username
                .attr('href', '')
                .html('')
        }
    }
};

var redraw = function (e, self, symbol, period) {
    var event = e || window.event;
    event.preventDefault();
    period = period || 'month';

    // period selector
    $('#price-chart-' + symbol).find('a').removeClass('active');
    $(self).addClass('active');

    d3.json('https://coinkapp.com/chart/price/' + symbol + '/' + period, function (error, response) {
        var selector = "#price-" + symbol;
        var svg = d3.select(selector).select('svg');
            svg.selectAll("*").remove();

        var canvas = d3.select(selector).select('canvas');
        canvas.remove();

        canvas = d3.select(selector).append("canvas")
            .attr("width", width + margin.left + margin.right)
            .attr("height", avatarSize)
            .style("margin-top", '1px')
        ;

        if (response.length === 0) {
            return;
        }

        var focus = svg.append("g")
            .attr("class", "focus")
            .style("display", "none");

        focus.append("circle")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .attr("r", 4.5);

        var tooltipText = focus.append("text");

        tooltipText.append("tspan")
            .attr("font-size", '0.8em')
            .attr("x", 9)
            .attr("dy", "0em");

        tooltipText.append("tspan")
            .attr("font-size", '0.8em')
            .attr("x", 9)
            .attr("dy", ".85em");

        draw(selector, response)
    });
};

var drawAvatar = function (x, context, data) {
    var img = new Image();
    img.src = data[0];
    img.onload = (function() {
        var t = img;
        return function() {
            context.save();
            context.strokeStyle = '#FFFFFF';
            context.lineWidth= 2;
            context.drawImage(t, x, 0, avatarSize, avatarSize);
            context.strokeRect(x, 0, avatarSize, avatarSize);
            context.restore();
        };
    })(img);
};
